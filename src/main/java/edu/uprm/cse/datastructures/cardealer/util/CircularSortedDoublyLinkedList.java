package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {
	
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		
		private Node<E> nextNode;
		
		public CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
		}

		/*
		 * Checks if the iterator next node is not the header.
		 * 
		 * @return true if the next node doesn't equals the header otherwise returns false.
		 */
		@Override
		public boolean hasNext() {
			return nextNode != header;
		}

		/*
		 * If there is a next node gets the next node element to iterate thought the list .
		 * 
		 * @return the element of the next node or NoSuchElementException if the is no next node.
		 */
		@Override
		public E next() {
			if(this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
	}
	
	private static class Node<E>{
		private E element;
		private Node<E> next;
		private Node<E> prev;
		
		public Node(E element, Node<E> next, Node<E> prev) {
			this.element = element;
			this.next = next;
			this.prev = prev;
		}
		
		public Node() {
			this.element = null;
			this.next = null;
			this.prev = null;
		}
		
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public Node<E> getPrev() {
			return prev;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}
	}

	private Node<E> header;
	private int currentSize;
	private Comparator comparator;
	
	public CircularSortedDoublyLinkedList(Comparator<E> carComparator) {
		this.currentSize = 0;
		this.header = new Node<>();
		this.header.setNext(this.header);
		this.header.setPrev(this.header);
		this.comparator = carComparator;
	}
	
	//CircularSortedDoublyLinkedListIterator
	@Override
	public Iterator<E> iterator() {
		 return new CircularSortedDoublyLinkedListIterator<E>();
	}
	
	/*
	 * Adds the object to the list if is empty else calls the comparator to add the object while sorting the list.
	 * 
	 * @return true after adding the object.
	 */
	@Override
	public boolean add(E obj) {
		if(this.isEmpty()) {
			Node<E> newNode = new Node<E>(obj, this.header, this.header);
			this.header.setNext(newNode);
			this.header.setPrev(newNode);
			this.currentSize++;
			return true;
		}else {
			Node<E> temp = this.header.getNext();
			while(temp.getElement() !=  null) {
				if(this.comparator.compare(obj, temp.getElement()) <= 0) {
					Node<E> newNode = new Node<E>(obj, temp, temp.getPrev());
					temp.getPrev().setNext(newNode);
					temp.setPrev(newNode);
					this.currentSize++;
					return true;
				}
				temp = temp.getNext();
			}
			Node<E> newNode = new Node<E>(obj, this.header, this.header.getPrev());
			this.header.getPrev().setNext(newNode);
			this.header.setPrev(newNode);
			this.currentSize++;
			return true;
		}
	}
	
	//returns the size of the list.
	@Override
	public int size() {
		return this.currentSize;
	}
	
	/*
	 * Removes the target object from the list.
	 * 
	 * @return true if the object was remove otherwise returns false.
	 */
	@Override
	public boolean remove(E obj) {
		if(!this.isEmpty()) {
			int index = this.firstIndex(obj);
			if(index >= 0) {
				return this.remove(index);
			}
			else {
				return false;
			}
		}
		return false;
	}

	/*
	 * Removes the target object at the specify position.
	 * 
	 * @return true if the object was remove or a IndexOutOfBoundsException if the index is invalid.
	 */
	@Override
	public boolean remove(int index) {
		Node<E> newNode = this.getPosition(index);
		newNode.getPrev().setNext(newNode.getNext());
		newNode.getNext().setPrev(newNode.getPrev());
		newNode.setElement(null);
		newNode.setNext(null);
		newNode.setPrev(null);
		this.currentSize--;
		return true;
	}

	/*
	 * Removes all the object from the list.
	 * 
	 * @return the number of objects remove.
	 */
	@Override
	public int removeAll(E obj) {
		int count = 0;
		while(this.contains(obj)) {
			this.remove(obj);
			count++;
		}
		return count;
	}
	
	//Returns the first element in the list.
	@Override
	public E first() {
		return this.header.getNext().getElement();
	}

	//Return the last element in the list.
	@Override
	public E last() {
		return this.header.getPrev().getElement();
	}

	/*
	 * Gets the element at the specify position.
	 * 
	 * @return the element at the specify position or IndexOutOfBoundsException if the index is invalid.
	 */
	@Override
	public E get(int index) {
		Node<E> temp = this.getPosition(index);
		return temp.getElement();
	}
	
	/*
	 * Auxiliary method
	 * 
	 * Gets the node at the specify position.
	 * 
	 * @return the node at the specify position or IndexOutOfBoundsException if the index is invalid.
	 */
	private Node<E> getPosition(int index){
		if((index < 0) || (index > this.currentSize)) {
			throw new IndexOutOfBoundsException();
		}
		Node<E> temp = this.header.getNext();
		int currentPosition = 0;
		while(currentPosition != index) {
			temp = temp.getNext();
			currentPosition++;
		}
		return temp;
	}

	//Clears all elements in the list.
	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}

	/*
	 * Checks if the element is in the list.
	 * 
	 * @return true if the element is in the list otherwise return false.
	 */
	@Override
	public boolean contains(E e) {
		return this.firstIndex(e) >= 0;
	}

	//Checks if the list is empty.
	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	/*
	 * Gets the position of first occurrence of the target object.
	 * 
	 * @return the position of the first occurrence of the target object.
	 */
	@Override
	public int firstIndex(E e) {
		int index = 0;
		for(Node<E> temp = this.header.getNext(); temp != this.header; temp = temp.getNext(), ++index) {
			if(temp.getElement().equals(e)) {
				return index;
			}
		}
		return -1;
	}

	/*
	 * Gets the position of last occurrence of the target object.
	 * 
	 * @return the position of the last occurrence of the target object.
	 */
	@Override
	public int lastIndex(E e) {
		int index = this.currentSize -1;
		for(Node<E> temp = this.header.getPrev(); temp != this.header; temp = temp.getPrev(), --index) {
			if(temp.getElement().equals(e)) {
				return index;
			}
		}
		return -1;
	}
}
