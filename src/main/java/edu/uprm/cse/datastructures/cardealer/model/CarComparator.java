package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{

	/*
	 * Compares the cars by car brand, model and model option.
	 * 
	 * @return IllegalStateException if c1 and c2 are not instances of Car.
	 * 
	 * @return 1 if c1 is > c2, 0 if c1 = c2 or -1 if c1 < c2.
	 */
	@Override
	public int compare(Car c1, Car c2) {
		
		if(!(c1 instanceof Car) && !(c2 instanceof Car)) {
			throw new IllegalStateException();
		}
		
		if(c1.getCarBrand().compareTo(c2.getCarBrand()) == 0) {
			if(c1.getCarModel().compareTo(c2.getCarModel()) == 0) {
				if(c1.getCarModelOption().compareTo(c2.getCarModelOption()) == 0) {
					return 0;
				}
				else {
					return c1.getCarModelOption().compareTo(c2.getCarModelOption());
				}
			}
			else {
				return c1.getCarModel().compareTo(c2.getCarModel());
			}
		}
		return c1.getCarBrand().compareTo(c2.getCarBrand());
	}
}
