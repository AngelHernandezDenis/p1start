package edu.uprm.cse.datastructures.cardealer.util;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CarList {
	
	//Creates a new CircularSortedDoublyLinkedList using CarComparator
	public static CircularSortedDoublyLinkedList<Car> CarList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	
	//Creates an instance of the list
	public static CircularSortedDoublyLinkedList<Car> getInstance() {
		return CarList;
	}
	
	//Resets the list of cars
	public static void resetCars() {
		CarList.clear();
	}
}
